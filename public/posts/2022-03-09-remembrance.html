<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Do You Want To Be Remembered By The Software You Write?</title>
    <link rel="stylesheet" href="../style.css">
</head>
<body>
<div class="navbar">
    <a href="https://dbaum1.gitlab.io/personal-site/">Home</a>
    <a href="../links.html">Links</a>
    <a href="https://gitlab.com/DBaum1/personal-site">Site source code</a>
    <a href="https://gitlab.com/DBaum1/">Repository</a>
</div>
<h1>Do You Want To Be Remembered By The Software You Write?</h1>
<p>
    Early June 2021, I decided to finish watching The Queen's Gambit.
    I'm not normally a TV watcher<a id="footnote-1-ref" href="#footnote-1"><sup>[1]</sup></a>
    so this was about 6 months after everyone else had already finished watching it.
    While most people (and YouTube clips) concentrate on the chess games, it was one short
    conversation between Beth Harmon and her ex-chess competitor, Harry Beltik that proved most memorable
    for me; so memorable that even after I watched it in a sleep-deprived state it's still on my mind more than
    half a year later.
</p>
<p>
    Harry, having learned of Beth's defeat by the Soviet chess champion, Borgov,
    and determined to reconnect with her after years apart, visits Beth's apartment to mentor her. Harry, once
    a state chess champion with ambitions to become a grand master, now works at a supermarket while he goes
    to college studying electrical engineering. But he hasn't completely neglected chess; he can still put
    up a respectable fight. Even so, he quickly discovers that while he's been focusing on college,
    Beth's chess knowledge has completely surpassed his and her single-minded determination to defeat Borgov
    has resulted in an almost <em>complete inability</em> to focus on anything non chess related, culminating in
    the following exchange:
</p>
<figure>
    <blockquote>
        <strong>Harry Beltik:</strong> I guess you've helped me too. You've helped me realize something.
        <br>
        <strong>Beth Harmon:</strong> What?
        <br>
        <strong>Harry Beltik:</strong> That I don't love chess. No, it's ok. I just don't love it as much
        as I once did. I'm not obsessed with it the way one has to be to win it all. The way you are.
    </blockquote>
    <figcaption>—The Queen's Gambit, <cite>Fork</cite></figcaption>
</figure>
<p>
    I really identified with this scene because while I desperately want to become Beth Harmon,
    I'm forced to confront the reality that in my current daily life and way of acting, I'm really
    a Harry Beltik. Let me explain a bit further.
</p>
<p>
    I write software for a living.
    Fundamentally I like to think of myself as a developer and aspiring innovator, someone who will one day
    create something - from scratch, code written on behalf of and owned by other people or institutions doesn't count
    - that's useful enough to be worth keeping alive even when I'm no longer interested or able to do so myself.
    Call it "code that's worth maintaining after I'm dead." A modern
    Thucydides writing about famous programmers would have to make some modifications to his writing:
</p>
<figure>
    <blockquote>
        For famous [programmers] have the whole earth as their memorial: it is not only the [code]
        on their [computers] in their own [possession] that mark them out; no, in foreign lands also, not in any
        visible form but in [the line between technology and magic], their [impact] abides and grows. It is for you to try
        to be like them.
    </blockquote>
    <figcaption>—Thucydides, <cite>History of the Peloponnesian War</cite></figcaption>
</figure>
<p>
    If creating something impactful is important to me, I picked perhaps the single best field for it:
    the barrier to entry of creating software in the modern era is as close to zero as you can get for an
    activity that can potentially impact billions: the only startup expenses<a id="footnote-2-ref" href="#footnote-2"><sup>[2]</sup></a>
    are the cost of whatever computer you're using, some peripherals like a mouse and keyboard
    (if you want to get fancy, throw in a new desk and chair), a reliable electrical and internet
    connection,<a id="footnote-3-ref" href="#footnote-3"><sup>[3]</sup></a>
    maybe a reference book or two if you want to go old school. The only additional costs are if
    you're developing for a specific hardware or using a proprietary software suite that doesn't have an
    adequate free version.<a id="footnote-4-ref" href="#footnote-4"><sup>[4]</sup></a> Unlike other high impact
    professions like law and medicine, software developers don't need to spend the equivalent of a down payment on
    a home on professional school before being legally allowed to practice.<a id="footnote-5-ref" href="#footnote-5"><sup>[5]</sup></a>
</p>
<p>
    It's precisely this low barrier to entry that gives me the most guilt over my lack of action, my neglect
    of various personal project ideas, lack of open source contributions, code I know I'm capable of writing,
    if only to test my mettle and improve my skills.
    I sometimes say that if I couldn't have studied computer science I'd have studied computer or electrical
    engineering instead and if those weren't available, mechanical or civil engineering. Had I studied any of those,
    I'd feel less sad simply because the barrier to creation in those fields is so much higher: if you
    know how to start a hardware factory, engineering firm, or construction company on a budget, call me. No one
    can fault you for not being able to start a new CPU fabrication company, but when all I need to do is
    start writing Java, C, Python, insert-whatever-language-you-want to accomplish my goals what excuse do I have?
</p>
<p>
    It's not that I have some Silicon Valley startup fantasy. I've got <em>zero or even negative</em> interest in trying to
    become the next Big Tech billionaire. I'm simply not a corporate ladder climber
    nor do I wish to turn myself into one. The only way I'm getting to the top of any corporation is if I'm the
    founder, and any company I'm heading will be a niche, mission based private company rather than a
    traditional startup with the sole goal of going public and becoming the next Google (or more likely, being
    acquired by Google).
</p>
<p>
    If I could only name one tech role model, it'd be Linus Torvalds. If I could create anything even a
    hundredth as successful as the Linux kernel,<a id="footnote-6-ref" href="#footnote-6"><sup>[6]</sup></a>
    I'd consider my mission fulfilled.
    But I suspect that in order to create anything that comes anywhere close to that level of impact,
    you have to be willing to dedicate an unfathomable amount of time to it, because you really and
    truly love it and can't see yourself doing anything except for that, much like Beth Harmon and her
    love of chess. And at the moment, such a level of dedication seems unattainable for me. I haven't
    found any idea that's worth pursuing to that level. Like Harry Beltik, I'm just not obsessed with it
    the way one has to be to win it all.
</p>
<h4>Footnotes</h4>
<ol>
    <li id="footnote-1">I've gone the past 5 years without actively turning on a TV to watch something
        (unless you count occasionally being tech support for family members' TVs or during watch party social
        situations). And I don't have subscriptions to streaming services like Netflix or Disney+ either. The TV
        in my small apartment hasn't been turned on since the day I moved in.<a href="#footnote-1-ref"> &#8617&#xFE0E; [1]</a></li>
    <li id="footnote-2">Obviously if your software revolves around also directly offering
        physical products and/or human services you'll require significant startup capital for the additional products
        and people, but the additional monetary cost of writing the code once you've got the computer hardware
        is theoretically zero, at least until you've become successful enough to need to hire other people to
        help you code and turn your personal project into a full time job.<a href="#footnote-2-ref"> &#8617&#xFE0E; [2]</a></li>
    <li id="footnote-3">Strictly speaking an internet connection isn't needed to code - coding's existed for
        decades before the internet but since the numerous resources found on the internet make coding
        100x easier, it might as well be an official requirement. And that's before we get to distributing
        whatever software you write. You'd have to be an extreme Luddite to opt for physical distribution
        over electronic distribution. And the costs of physical distribution will still
        be more than putting it on some file sharing site.<a href="#footnote-3-ref"> &#8617&#xFE0E; [3]</a></li>
    <li id="footnote-4">Looking at you, Matlab.<a href="#footnote-4-ref"> &#8617&#xFE0E; [4]</a></li>
    <li id="footnote-5">Some developers are completely self taught.<a href="#footnote-5-ref"> &#8617&#xFE0E; [5]</a></li>
    <li id="footnote-6">Yes, I'm well aware that it's been a long time since Linus was the sole kernel
        developer. And that much of Linux wouldn't be possible without contributions originally from GNU (there's your obligatory mention,
        Richard Stallman!). The point is that he started it.<a href="#footnote-6-ref"> &#8617&#xFE0E; [6]</a></li>
</ol>
</body>
</html>