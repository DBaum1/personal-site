def is_integer(line):
    try:
        float(line)
    except ValueError:
        return False
    else:
        return float(line).is_integer()

def is_timestamp(line):
    if "-->" in line:
        return True
    return False
        
f = open("srt.txt", "r")
totalWords = 0

for line in f:
    x = line
    if (not is_integer(x) and not is_timestamp(x)):
        #print(line)
        words = len(x.split())
        totalWords += words

f.close()
print("done")
print("totalWords = " + str(totalWords))
